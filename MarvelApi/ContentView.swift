//
//  ContentView.swift
//  MarvelApi
//
//  Created by IGNACIO HERNAIZ IZQUIERDO on 15/3/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
